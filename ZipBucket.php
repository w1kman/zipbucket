<?php
namespace Ezpz\components;

class ZipBucket {
    /** Bitbucket URL */
    const BITBUCKET_URL = "https://bitbucket.org/<repository>/get/<branch><extension>";

    /** @var string Bitbucket username */
    private $_username;

    /** @var string Bitbucket password */
    private $_password;

    /** @var array Array of archive extensions */
    protected $_archiveExtension = [
        'zip' => '.zip',      // Default
        'gz' => '.tar.gz',
        'bz2' => '.tar.bz2'
    ];

    /** @var string Active path */
    protected $_activePath;

    /** @var string Downloaded filename */
    protected $_downloadedFilename;

    /** @var int cURL timeout in seconds */
    public $curlTimeout = 30;

    /** @var string Bitbucket repository ('account/repository') */
    public $repository = '';

    /** @var string Bitbucket repository branch (OR tag) */
    public $branch = 'master';

    /** @var string Archive type */
    public $archiveType = 'zip';

    /** @var string Destination structure to create (appended to destination directory) */
    /* public $destinationStructure = '<filedir>/downloads/<repository>/<branch>/<branch>_<date><extension>'; */
    public $destinationStructure = '<filedir>/downloads/<repository>/<branch>/archive<extension>'; /* DEVELOPMENT */

    /**
     * ZipBucket constructor
     *
     * @param string $repository
     * @param null|string $username
     * @param null|string $password
     */
    public function __construct($repository, $username = null, $password = null) {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $this->repository = $repository;
        $this->setUsername($username);
        $this->setPassword($password);
    }

    /**
     * Set _username
     *
     * @param null|string $username
     * @return ZipBucket
     */
    public function setUsername($username = null) {
        $this->_username = $username;

        return $this;
    }

    /**
     * Set _password
     *
     * @param null|string $password
     * @return ZipBucket
     */
    public function setPassword($password = null) {
        $this->_password = $password;

        return $this;
    }

    /**
     * Returns real destination path from destination
     *
     * @return string
     */
    public function getDestinationPath() {
        return $this->_replaceTags($this->destinationStructure);
    }

    /**
     * Returns Bitbucket download url
     *
     * @return string
     */
    public function getBitbucketUrl() {
        return $this->_replaceTags(self::BITBUCKET_URL);
    }

    /**
     * Replace tags with values
     *
     * @param string $string
     * @return string
     */
    public function _replaceTags($string) {
        $tags = [
            '<filedir>' => dirname(__FILE__),
            '<repository>' => $this->repository,
            '<branch>' => $this->branch,
            '<date>' => date('YmdHis', time()),
            '<extension>' => $this->_archiveExtension[$this->archiveType]
        ];

        return str_replace(array_keys($tags), array_values($tags), $string);
    }

    /**
     * Create directory if it does not already exist.
     * TODO: Tests on Mac and Linux
     *
     * @param string $path
     * @return ZipBucket
     * @throws \Exception
     */
    protected function _createDirectory($path) {
        $parts = explode('/', $path);
        array_pop($parts);
        $directory = implode('/', $parts);
        if (!is_dir($directory)) {
            mkdir($directory, 0777, true);
            if (!is_dir($directory)) {
                throw new \Exception('Unable to create destination directory.', 500);
            }
        }

        return $this;
    }

    /**
     * Create folder structure and file pointer resource
     *
     * @return resource
     * @throws \Exception
     */
    protected function createDestinationFile() {
        $path = $this->getDestinationPath();
        $this->_activePath = $path;
        $this->_createDirectory($path);

        return fopen($path, 'w');
    }

    /**
     * Get repository!
     *
     * @return array
     */
    public function download() {
        $filePointer = $this->createDestinationFile();
        $result = $this->_execCurl($filePointer);
        fclose($filePointer);
        if ($result['curl'] === false && !empty($this->_activePath)) {
            $result['unlink'] = unlink($this->_activePath);
        } else {
            $this->_downloadedFilename = $this->_activePath;
        }
        $this->_activePath = null;

        return $result;
    }

    /**
     * Display downloaded archive content
     */
    public function getArchiveInfo() {
        $zipArchive = new \ZipArchive();
        $zipArchive->open($this->_downloadedFilename);
        $this->_removeRepositoryDirectory($zipArchive);
        $out = [];
        for ($i = 0; $i < $zipArchive->numFiles; $i++) {
            echo $i . PHP_EOL;
            $stat = $zipArchive->statIndex($i);
            $out[] = print_r($stat);
        }

        return $out;
    }

    public function extractTo($directoryName = null, $destination = './package/') {
        $zipArchive = new \ZipArchive();
        $zipArchive->open($this->_downloadedFilename);
        $packageDirectory = $zipArchive->statIndex(0)['name'];

        $zipArchive->extractTo($destination);
        $zipArchive->close();
        $extractedPath = $destination . $packageDirectory;
        if ($directoryName !== null && $directoryName !== $packageDirectory) {
            $extractedPath = $this->_renamePackageDirectory($packageDirectory, $directoryName, $destination);
        }
    }

    /**
     * Rename directory (or file)
     *
     * @param string $from File/Directory to rename
     * @param string $to New name
     * @param string $prependPath when not empty this value will be prepended to from and to
     * @param bool $replace Whether or not to replace already existing directories
     * @param bool $forceDeleteOnWin
     * @return bool
     * @throws \Exception
     */
    public function _renamePackageDirectory($from, $to, $prependPath = '', $replace = true, $forceDeleteOnWin = true) {
        $from = $prependPath . $from;
        $to = $prependPath . $to;
        $fromExists = (is_dir($from) || file_exists($from));
        $toExists = (is_dir($to) || file_exists($to));
        /* Can we, should we rename... */
        chmod($to, 0777);
        if (!$fromExists) {
            throw new \Exception('Source file/directory does not exist.', 500);
        } elseif ($toExists && !$replace) {
            throw new \Exception('Target file/directory already exists.', 500);
        } elseif ($toExists && strtolower(substr(PHP_OS, 0, 3)) === 'win') {
            /* PHP + Windows = unwanted results, here is the "fix" */
            if ($forceDeleteOnWin === true && !unlink($to)) {
                throw new \Exception('WINDOWS: Unfortunately its not possible to use rename when target directory exists. Try forceDeleteOnWin.', 500);
            } else {
                throw new \Exception('WINDOWS: Unfortunately its not possible to use rename when target directory exists. Try forceDeleteOnWin.', 500);
            }
        }

        /* Rename */
        return rename($from, $to);

    }

    /**
     * Execute cURL to get archive
     *
     * @param resource $filePointer
     * @return array
     */
    protected function _execCurl(&$filePointer) {
        $cURL = curl_init();
        $options = [
            CURLOPT_URL => $this->getBitbucketUrl(),
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_TIMEOUT => $this->curlTimeout,
            CURLOPT_RETURNTRANSFER => false,
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_USERPWD => $this->_username . ':' . $this->_password,
            CURLOPT_FILE => $filePointer
        ];
        /* If we don't have a username, don't login */
        if (empty($this->_username)) {
            unset($options[CURLOPT_HTTPAUTH], $options[CURLOPT_USERPWD]);
        }
        /* Apply cURL options */
        curl_setopt_array($cURL, $options);
        $result = curl_exec($cURL);
        $httpStatus = curl_getinfo($cURL, CURLINFO_HTTP_CODE);
        $cURLErrno = curl_errno($cURL);
        curl_close($cURL);

        return [
            'curl' => $result,
            'httpStatus' => $httpStatus,
            'curlError' => $cURLErrno
        ];
    }
}

/* Example */
echo "<pre>";
$umask = umask();
$zipBucket = new ZipBucket('w1kman/zipbucket');
$result = $zipBucket->download();
$zipBucket->extractTo('ZipBucket');
umask($umask);
var_dump($result);
